# Nanino crcmkr

Version de la placa que hizo Robelix de la pcb Nanino (arduino) de Johan von Konow.
Esta placa ofrece unas pistas mas anchas , un minimo de 0.7 mm o 0.75mm . Estan pensadas para 
realizarla en una cnc #diy , en concreto en la cyclone pcb factory.

![render1](/Imagenes/nanino1.png)
![render1](/Imagenes/nanino2.png)
![render1](/Imagenes/nanino3.png)